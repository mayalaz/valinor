import { Test, TestingModule } from '@nestjs/testing';
import { NestPgpromiseModule } from 'nestjs-pgpromise';
import { AuthorsService } from './authors.service';
import { IAuthor } from './authors.model';
import { ConfigModule } from '@nestjs/config';

describe('AuthorsService', () => {
  let service: AuthorsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot({
          envFilePath: '.env.dev',
        }),
        NestPgpromiseModule.register({
          connection: {
            host: `${process.env.DB_HOST}`,
            port: Number(process.env.DB_PORT),
            database: `${process.env.DB_NAME}`,
            user: `${process.env.DB_USERNAME}`,
            password: `${process.env.DB_USERPASSWORD}`,
          },
        }),
      ],
      providers: [AuthorsService],
    }).compile();

    service = module.get<AuthorsService>(AuthorsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('list', () => {
    it('should return a list of authors', async () => {
      const object: IAuthor = {};
      const authors = await service.list();

      expect(authors).toEqual(expect.arrayContaining([expect.objectContaining(object)]));
    });
  });
});
