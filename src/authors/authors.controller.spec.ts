import { ConfigModule } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { NestPgpromiseModule } from 'nestjs-pgpromise';
import { AuthorsController } from './authors.controller';
import { IAuthor } from './authors.model';
import { AuthorsService } from './authors.service';

describe('AuthorsController', () => {
  let controller: AuthorsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot({
          envFilePath: '.env.dev',
        }),
        NestPgpromiseModule.register({
          connection: {
            host: `${process.env.DB_HOST}`,
            port: Number(process.env.DB_PORT),
            database: `${process.env.DB_NAME}`,
            user: `${process.env.DB_USERNAME}`,
            password: `${process.env.DB_USERPASSWORD}`,
          },
        }),
      ],
      controllers: [AuthorsController],
      providers: [AuthorsService],
    }).compile();

    controller = module.get<AuthorsController>(AuthorsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('getList', () => {
    it('should return a list of authors', async () => {
      const object: IAuthor = {};
      const authors = await controller.getList();

      expect(authors).toMatchObject(expect.arrayContaining([expect.objectContaining(object)]));
    });
  });
});
