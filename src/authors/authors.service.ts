import { Inject, Injectable } from '@nestjs/common';
import { NEST_PGPROMISE_CONNECTION } from 'nestjs-pgpromise';
import { IDatabase } from 'pg-promise';
import { IAuthor } from './authors.model';

@Injectable()
export class AuthorsService {
  constructor(@Inject(NEST_PGPROMISE_CONNECTION) private readonly cnn: IDatabase<any>) {}

  async list(): Promise<IAuthor[]> {
    const query = 'select * from author';
    return this.cnn.manyOrNone<IAuthor>(query);
  }
}
