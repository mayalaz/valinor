export interface IAuthor {
  id?: string;
  first_name?: string;
  last_name?: string;
}
