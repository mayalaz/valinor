import { Controller, Get, InternalServerErrorException, Logger } from '@nestjs/common';
import { AuthorsService } from './authors.service';

@Controller('api/authors')
export class AuthorsController {
  private logger = new Logger('controller');

  constructor(private readonly authorService: AuthorsService) {}

  @Get()
  async getList() {
    try {
      const authors = await this.authorService.list();
      return authors;
    } catch (err) {
      this.logger.log(err);
      throw new InternalServerErrorException(err);
    }
  }
}
