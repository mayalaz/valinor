import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { NestPgpromiseModule } from 'nestjs-pgpromise';
import { HelloWorldModule } from './hello-world/hello-world.module';
import { AuthorsModule } from './authors/authors.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: '.env.dev',
    }),
    NestPgpromiseModule.register({
      connection: {
        host: `${process.env.DB_HOST}`,
        port: Number(process.env.DB_PORT),
        database: `${process.env.DB_NAME}`,
        user: `${process.env.DB_USERNAME}`,
        password: `${process.env.DB_USERPASSWORD}`,
      },
    }),
    HelloWorldModule,
    AuthorsModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
