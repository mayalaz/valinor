import { Injectable } from '@nestjs/common';

@Injectable()
export class HelloWorldService {
  helloWorld(): string {
    return 'Hello World!';
  }
}
