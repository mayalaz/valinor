# Valinor - Simple API

Project created with NestJS framework for NodeJS. It is written in Typescript and connects to an external database that has information about horror book authors.

---

### **Prerequisites**

#### **NodeJS**

You can find more information about the installation on the [official Node.js website](https://nodejs.org/) and the [official NPM website](https://npmjs.org/).

#### **NestJS**

```
$ npm i -g @nestjs/cli
```

---

### **Install**
```
$ git clone https://gitlab.com/mayalaz/valinor
$ cd valinor
$ npm install
```

---

### **Unit testing**

```
npm run test
```

### **E2E testing**

```
npm run test:e2e
```

---

### **Run**

```
npm run start
```

*Development mode*
```
npm run start:dev
```

---

### **Build**
```
npm run build
```

---

### **Available endpoints**

*Hello World!*

```
http://localhost:3000/api/hello-world
```

*List of horror authors*
```
http://localhost:3000/api/authors
```
