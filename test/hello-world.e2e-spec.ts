import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { HelloWorldModule } from './../src/hello-world/hello-world.module';

describe('HelloWorldController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [HelloWorldModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('GET /api/hello-world', () => {
    return request(app.getHttpServer()).get('/api/hello-world').expect(200).expect('Hello World!');
  });
});
