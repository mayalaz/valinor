import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AuthorsModule } from '../src/authors/authors.module';
import { ConfigModule } from '@nestjs/config';
import { NestPgpromiseModule } from 'nestjs-pgpromise';
import { IAuthor } from 'src/authors/authors.model';

describe('AuthorsController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        AuthorsModule,
        ConfigModule.forRoot({
          envFilePath: '.env.dev',
        }),
        NestPgpromiseModule.register({
          connection: {
            host: `${process.env.DB_HOST}`,
            port: Number(process.env.DB_PORT),
            database: `${process.env.DB_NAME}`,
            user: `${process.env.DB_USERNAME}`,
            password: `${process.env.DB_USERPASSWORD}`,
          },
        }),
      ],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('GET /api/authors', () => {
    const object: IAuthor = {};

    return request(app.getHttpServer())
      .get('/api/authors')
      .expect(200)
      .then((response) => {
        expect(response.body).toEqual(expect.arrayContaining([expect.objectContaining(object)]));
      });
  });
});
